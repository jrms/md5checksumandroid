package com.jrms.md5checksum.tasks

import android.os.AsyncTask
import com.jrms.md5checksum.utils.ChecksumFromString
import java.lang.ref.WeakReference

class GetChecksumFromStringTask(val algorithm : Int, var onGetChecksumWeak: WeakReference<OnGetChecksum?>) : AsyncTask<String, Void, String?>(){

    interface OnGetChecksum{
        fun onPostExecute(checksum : String, algorithm : Int)
    }

    override fun doInBackground(vararg p0: String?): String? {
        val checksumFromString = ChecksumFromString()
        var checksumString : String? = null
        when(algorithm){
            1 -> {
                checksumString = checksumFromString.getMD5Hash(p0[0]!!)
            }
            2 -> {
                checksumString = checksumFromString.getSHA1Hash(p0[0]!!)
            }
            3 -> {
                checksumString = checksumFromString.getSHA256Hash(p0[0]!!)
            }
            4 -> {
                checksumString = checksumFromString.getSHA384Hash(p0[0]!!)
            }

            5 -> {
                checksumString = checksumFromString.getSHA512Hash(p0[0]!!)
            }
            6 -> {
                checksumString = checksumFromString.getCRC32(p0[0]!!)
            }
            7 -> {
                checksumString = checksumFromString.getAdler32(p0[0]!!)
            }
        }
        return checksumString
    }

    override fun onPostExecute(result: String?) {
        val onGetChecksum = onGetChecksumWeak.get()
        onGetChecksum?.onPostExecute(result!!, algorithm)
    }
}