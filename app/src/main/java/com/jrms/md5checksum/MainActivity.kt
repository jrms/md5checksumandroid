package com.jrms.md5checksum

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.jrms.md5checksum.services.ChecksumReceiver
import com.jrms.md5checksum.services.ChecksumService
import com.jrms.md5checksum.utils.Utils
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import android.content.DialogInterface
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AlertDialog
import com.jrms.md5checksum.adapters.FragmentStatePagerChecksum
import com.jrms.md5checksum.fragments.FragmentChecksumFile
import com.jrms.md5checksum.fragments.FragmentChecksumString
import com.jrms.md5checksum.tasks.GetChecksumFromStringTask


class MainActivity : AppCompatActivity(), GetChecksumFromStringTask.OnGetChecksum, ViewPager.OnPageChangeListener {


    lateinit var checksumReceiver : ChecksumReceiver
    var menuItem : MenuItem? = null


    companion object {
        const val CHECKSUM_ALGORITHM = "checksum_algorithm"
        const val FILE_URI = "file_uri"
        const val HASH = "hash"
        const val CHANNEL_ID = "md5ChecksumidChannel"
        const val PICK_FILE_CODE: Int = 0x01
        const val CHECKSUM_SERVICE_ID = 0x05
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar.setTitleTextColor(ContextCompat.getColor(this, android.R.color.white))
        setSupportActionBar(toolbar)
        registerChecksumReceiver()

        createNotificationChannel()

        val arrayTag = arrayOf(resources.getString(R.string.file), resources.getString(R.string.string))
        pagerChecksum.adapter = FragmentStatePagerChecksum(supportFragmentManager, arrayTag)
        pagerChecksum.addOnPageChangeListener(this)

        tabLayoutChecksum.setupWithViewPager(pagerChecksum)


    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.notification_channel_name)
            val description = getString(R.string.notification_channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance)
            channel.description = description
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager!!.createNotificationChannel(channel)
        }
    }

    private fun registerChecksumReceiver() {
        var intentFilter = IntentFilter(ChecksumService.PROGRESS_ACTION)

        checksumReceiver = ChecksumReceiver(this)
        LocalBroadcastManager.getInstance(this).registerReceiver(checksumReceiver, intentFilter)

        intentFilter = IntentFilter(ChecksumService.END_PROCESS_ACTION)
        LocalBroadcastManager.getInstance(this).registerReceiver(checksumReceiver, intentFilter)
    }




    fun changeProgress(checksumAlgorithm: Int, progress: Int) {
        when (checksumAlgorithm) {

            1 -> {
                md5Progress.progress = progress
            }
            2 -> {
                sha1Progress.progress = progress
            }
            3 -> {
                sha256Progress.progress = progress
            }
            4 -> {
                sha384Progress.progress = progress
            }

            5 -> {
                sha512Progress.progress = progress
            }
            6 -> {
                crc32Progress.progress = progress
            }
            7 -> {
                adler32Progress.progress = progress

            }
        }
    }

    fun finishProcess(checksumAlgorithm: Int, result: String?) {
        val fragmentChecksum = (pagerChecksum.adapter as FragmentStatePagerChecksum ).fragmentSparse.get(0) as FragmentChecksumFile
        fragmentChecksum.finishProcess(checksumAlgorithm, result)

    }




    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        menuItem = menu?.findItem(R.id.selectFileOption)
        if(pagerChecksum.currentItem == 1){
            menuItem?.isVisible = false
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.selectFileOption -> {
                val fragmentChecksum = (pagerChecksum.adapter as FragmentStatePagerChecksum ).fragmentSparse.get(0) as FragmentChecksumFile
                fragmentChecksum.getUrl()
            }
        }
        return true
    }






    override fun onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(checksumReceiver)
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
        super.onDestroy()
    }

    override fun onBackPressed() {
        if(FragmentChecksumFile.running) {
            val builder = AlertDialog.Builder(this)
                    .setTitle(R.string.exitTitle)
                    .setMessage(R.string.exitMessage)
                    .setNegativeButton(R.string.no) { _: DialogInterface?, _: Int -> }
                    .setPositiveButton(R.string.yes) { _: DialogInterface?, _: Int ->
                        run {
                            ChecksumService.CANCEL = true
                            super.onBackPressed()
                        }
                    }
            builder.show()
        }else{
            super.onBackPressed()
        }

    }


    override fun onPostExecute(checksum: String, algorithm: Int) {
        val fragmentChecksumString = (pagerChecksum.adapter as FragmentStatePagerChecksum).fragmentSparse.get(1) as FragmentChecksumString
        fragmentChecksumString.setChecksumResult(checksum, algorithm)
    }


    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        if(position == 1){
            menuItem?.isVisible = false
        }else if(position == 0){
            menuItem?.isVisible = true
        }
    }





    fun copyToClipboard(view : View){
        val text = (view as TextView).text.toString()
        if(text != ""){
            Utils.copyToClipboard(this, text)
        }
    }

}
