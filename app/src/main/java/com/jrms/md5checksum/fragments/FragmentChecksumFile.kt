package com.jrms.md5checksum.fragments


import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.JobIntentService
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.jrms.md5checksum.MainActivity

import com.jrms.md5checksum.R
import com.jrms.md5checksum.services.ChecksumService
import com.jrms.md5checksum.utils.Utils
import com.jrms.md5checksum.utils.Utils.displayToast
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.content_main.view.*
import kotlinx.android.synthetic.main.fragment_fragment_checksum_file.view.*


class FragmentChecksumFile : Fragment() {

    companion object {
        var running = false
    }

    var fileSelected: Uri? = null


    private var greaterProcess = 1
    private var toast: Toast? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_fragment_checksum_file, container, false)
        // Inflate the layout for this fragment

        if (savedInstanceState != null) {
            val uri = savedInstanceState.getString("uri")
            if (uri != null) {
                fileSelected = Uri.parse(uri)
            }
            running = savedInstanceState.getBoolean("running", false)
            greaterProcess = savedInstanceState.getInt("greaterProcess", 1)

            view.md5Value.text = savedInstanceState.getString("md5")
            view.sha1Value.text = savedInstanceState.getString("sha1")
            view.sha256Value.text = savedInstanceState.getString("sha256")
            view.sha384Value.text = savedInstanceState.getString("sha384")
            view.crc32Value.text = savedInstanceState.getString("crc32")
            view.sha512Value.text = savedInstanceState.getString("sha512")
            view.adler32Value.text = savedInstanceState.getString("adler32")
            view.myHashEditText.setText(savedInstanceState.getString("myHash"))

            view.clearHashImageView.setOnClickListener{
                myHashEditText.text = null
            }
        }

        view.fab.setOnClickListener { _ ->
            if (fileSelected == null) {
                Toast.makeText(context, R.string.pleaseSelectAFile, Toast.LENGTH_SHORT).show()

            } else {
                if (running) {
                    displayToast(context, getString(R.string.waitProcessRunning), toast)

                } else {
                    resetProgress()
                    getChecksumIfSelected()
                }
            }

        }
        view.fileSelectedRouteEditText.setOnClickListener { _ ->
            getUrl()
        }
        return view
    }



    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("uri", fileSelected.toString())
        outState.putInt("greaterProcess", greaterProcess)
        outState.putBoolean("running", running)

        outState.putString("md5", md5Value.text.toString())
        outState.putString("sha1", sha1Value.text.toString())
        outState.putString("sha256", sha256Value.text.toString())
        outState.putString("sha384", sha384Value.text.toString())
        outState.putString("crc32", crc32Value.text.toString())
        outState.putString("sha512", sha512Value.text.toString())
        outState.putString("adler32", adler32Value.text.toString())
        outState.putString("myHash", myHashEditText.text.toString())


        myHashEditText.text = null
    }

    fun getUrl() {
        var fileChooser = Intent(Intent.ACTION_GET_CONTENT)
        fileChooser.type = "*/*"
        fileChooser = Intent.createChooser(fileChooser, getString(R.string.chooseFile))
        startActivityForResult(fileChooser, MainActivity.PICK_FILE_CODE)
    }

    private fun getChecksumIfSelected() {

        var checkedOne = false
        val intent = Intent()
        val algorithms = arrayListOf<Int>()
        if (enableMD5Checkbox.isChecked) {
            checkedOne = true
            algorithms.add(1)
            running = true
            greaterProcess = 1
        }
        if (enableSha1Checkbox.isChecked) {
            checkedOne = true
            algorithms.add(2)
            running = true
            greaterProcess = 2
        }
        if (enableSha256Checkbox.isChecked) {
            checkedOne = true
            algorithms.add(3)
            running = true
            greaterProcess = 3
        }
        if (enableSha384Checkbox.isChecked) {
            checkedOne = true
            algorithms.add(4)
            running = true
            greaterProcess = 4
        }
        if (enableSha512Checkbox.isChecked) {
            checkedOne = true
            algorithms.add(5)
            greaterProcess = 5

        }
        if (enablecrc32Checkbox.isChecked) {
            checkedOne = true
            algorithms.add(6)
            running = true
            greaterProcess = 6
        }
        if (enableadler32Checkbox.isChecked) {
            checkedOne = true
            algorithms.add(7)
            running = true
            greaterProcess = 7
        }

        if (!checkedOne) {
            Toast.makeText(context, R.string.notChecked, Toast.LENGTH_SHORT).show()
        }else{
            intent.putExtra(MainActivity.CHECKSUM_ALGORITHM, algorithms)
            intent.putExtra(MainActivity.FILE_URI, fileSelected.toString())
            ChecksumService.CANCEL = false
            JobIntentService.enqueueWork(context!!, ChecksumService::class.java, MainActivity.CHECKSUM_SERVICE_ID, intent)
        }
    }

    private fun resetProgress() {


        md5Progress.progress = 0
        sha1Progress.progress = 0
        sha256Progress.progress = 0
        sha384Progress.progress = 0
        sha512Progress.progress = 0
        crc32Progress.progress = 0
        adler32Progress.progress = 0


        md5Value.text = ""
        sha1Value.text = ""
        sha256Value.text = ""
        sha384Value.text = ""
        sha512Value.text = ""
        crc32Value.text = ""
        adler32Value.text = ""




    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == MainActivity.PICK_FILE_CODE && resultCode == Activity.RESULT_OK) {
            fileSelected = data!!.data
            println("Archivo seleccionado: " + fileSelected.toString())
            val fileMetadata = Utils.getFileMetadata(context, fileSelected)
            fileSelectedRouteEditText.setText(fileMetadata.name)

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    fun finishProcess(checksumAlgorithm : Int, result :String?){
        var algorithm : String? = null
        Log.w("ResultChecksum", "Number checksum algorithm " + checksumAlgorithm)
        when (checksumAlgorithm) {
            1 -> {
                md5Value.text = result
                running = 1 != greaterProcess
                algorithm = "MD5"

            }

            2 -> {
                sha1Value.text = result
                running = 2 != greaterProcess
                algorithm = "SHA-1"

            }
            3 -> {
                sha256Value.text = result
                running = 3 != greaterProcess
                algorithm = "SHA-256"

            }
            4 -> {
                sha384Value.text = result
                running = 4 != greaterProcess
                algorithm = "SHA-384"

            }

            5 -> {
                sha512Value.text = result
                running = 5 != greaterProcess
                algorithm = "SHA-512"

            }
            6 -> {
                crc32Value.text = result
                running = 6 != greaterProcess
                algorithm = "CRC32"

            }
            7 -> {
                adler32Value.text = result
                running = 7 != greaterProcess
                algorithm = "Adler32"

            }
        }


        if(myHashEditText.text.toString() != ""){
            val editTextResult = myHashEditText.text.toString()
            if(editTextResult == result){

                Toast.makeText(context, getString(R.string.hashMatches, algorithm), Toast.LENGTH_SHORT).show()
            }
        }
    }

}
