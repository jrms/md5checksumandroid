package com.jrms.md5checksum.fragments


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.jrms.md5checksum.R
import com.jrms.md5checksum.tasks.GetChecksumFromStringTask
import com.jrms.md5checksum.utils.Utils
import kotlinx.android.synthetic.main.content_string_digest.*
import kotlinx.android.synthetic.main.content_string_digest.view.*
import java.lang.ref.WeakReference

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FragmentChecksumString : Fragment(), View.OnClickListener {

    var onGetChecksum : GetChecksumFromStringTask.OnGetChecksum?= null

    private var toast: Toast? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val newView = inflater.inflate(R.layout.fragment_fragment_checksum_string, container, false)

        newView.buttonGetChecksum.setOnClickListener(this)


        if (savedInstanceState != null) {
            newView.md5Value.text = savedInstanceState.getString("md5")
            newView.sha1Value.text = savedInstanceState.getString("sha1")
            newView.sha256Value.text = savedInstanceState.getString("sha256")
            newView.sha384Value.text = savedInstanceState.getString("sha384")
            newView.sha512Value.text = savedInstanceState.getString("sha512")
            newView.crc32Value.text = savedInstanceState.getString("crc32")
            newView.adler32Value.text = savedInstanceState.getString("adler32")

            newView.md5Result.visibility = savedInstanceState.getInt("md5Visibility")
            newView.sha1Result.visibility = savedInstanceState.getInt("sha1Visibility")
            newView.sha256Result.visibility = savedInstanceState.getInt("sha256Visibility")
            newView.sha384Result.visibility = savedInstanceState.getInt("sha384Visibility")
            newView.sha512Result.visibility = savedInstanceState.getInt("sha512Visibility")
            newView.crc32Result.visibility = savedInstanceState.getInt("crc32Visibility")
            newView.alder32Result.visibility = savedInstanceState.getInt("adler32Visibility")


        }
        return newView
    }



    override fun onClick(v: View?) {
        resetResults()
        Utils.closeKeyboard(myString)

        lateinit var taskChecksum: GetChecksumFromStringTask
        var isSelectedOne = false

        if (md5Checkbox.isChecked) {
            isSelectedOne = true
            taskChecksum = GetChecksumFromStringTask(1, WeakReference(onGetChecksum))
            taskChecksum.execute(myString.text.toString())
        }
        if (sha1Checkbox.isChecked) {
            isSelectedOne = true
            taskChecksum = GetChecksumFromStringTask(2, WeakReference(onGetChecksum))
            taskChecksum.execute(myString.text.toString())
        }
        if (sha256Checkbox.isChecked) {
            isSelectedOne = true
            taskChecksum = GetChecksumFromStringTask(3, WeakReference(onGetChecksum))
            taskChecksum.execute(myString.text.toString())
        }
        if (sha384Checkbox.isChecked) {
            isSelectedOne = true
            taskChecksum = GetChecksumFromStringTask(4, WeakReference(onGetChecksum))
            taskChecksum.execute(myString.text.toString())
        }
        if (sha512Checkbox.isChecked) {
            isSelectedOne = true
            taskChecksum = GetChecksumFromStringTask(5, WeakReference(onGetChecksum))
            taskChecksum.execute(myString.text.toString())
        }
        if (crc32Checkbox.isChecked) {
            isSelectedOne = true
            taskChecksum = GetChecksumFromStringTask(6, WeakReference(onGetChecksum))
            taskChecksum.execute(myString.text.toString())
        }
        if (adler32Checkbox.isChecked) {
            isSelectedOne = true
            taskChecksum = GetChecksumFromStringTask(7, WeakReference(onGetChecksum))
            taskChecksum.execute(myString.text.toString())
        }

        if (!isSelectedOne) {
            Utils.displayToast(context, getString(R.string.notChecked), toast)
        }
    }

    fun setChecksumResult(checksum : String, algorithm : Int){
        when (algorithm) {
            1 -> {
                md5Result.visibility = View.VISIBLE
                md5Value.text = checksum
            }
            2 -> {
                sha1Result.visibility = View.VISIBLE
                sha1Value.text = checksum
            }
            3 -> {
                sha256Result.visibility = View.VISIBLE
                sha256Value.text = checksum
            }
            4 -> {
                sha384Result.visibility = View.VISIBLE
                sha384Value.text = checksum
            }

            5 -> {
                sha512Result.visibility = View.VISIBLE
                sha512Value.text = checksum
            }
            6 -> {
                crc32Result.visibility = View.VISIBLE
                crc32Value.text = checksum
            }
            7 -> {
                alder32Result.visibility = View.VISIBLE
                adler32Value.text = checksum
            }
        }
    }

    private fun resetResults() {

        md5Result.visibility = View.GONE
        md5Value.text = ""

        sha1Result.visibility = View.GONE
        sha1Value.text = ""

        sha256Result.visibility = View.GONE
        sha256Value.text = ""

        sha384Result.visibility = View.GONE
        sha384Value.text = ""

        sha512Result.visibility = View.GONE
        sha512Value.text = ""

        crc32Result.visibility = View.GONE
        crc32Value.text = ""

        alder32Result.visibility = View.GONE
        adler32Value.text = ""

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("md5", md5Value.text.toString())
        outState.putString("sha1", sha1Value.text.toString())
        outState.putString("sha256", sha256Value.text.toString())
        outState.putString("sha384", sha384Value.text.toString())
        outState.putString("sha512", sha512Value.text.toString())
        outState.putString("crc32", crc32Value.text.toString())
        outState.putString("adler32", adler32Value.text.toString())

        outState.putInt("md5Visibility", md5Result.visibility)
        outState.putInt("sha1Visibility",  sha1Result.visibility)
        outState.putInt("sha256Visibility",  sha256Result.visibility)
        outState.putInt("sha384Visibility", sha384Result.visibility)
        outState.putInt("sha512Visibility",  sha512Result.visibility)
        outState.putInt("crc32Visibility", crc32Result.visibility)
        outState.putInt("adler32Visibility",  alder32Result.visibility)

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        onGetChecksum = context as GetChecksumFromStringTask.OnGetChecksum
    }


    override fun onDetach() {
        onGetChecksum = null
        super.onDetach()
    }



}
