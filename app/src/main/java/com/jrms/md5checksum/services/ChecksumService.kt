package com.jrms.md5checksum.services


import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v4.app.JobIntentService
import android.support.v4.app.NotificationCompat
import android.support.v4.content.LocalBroadcastManager
import com.jrms.md5checksum.MainActivity
import com.jrms.md5checksum.R
import com.jrms.md5checksum.utils.Checksum
import com.jrms.md5checksum.utils.ChecksumFromFile


class ChecksumService : JobIntentService(), Checksum.OnPublish {


    lateinit var algorithms: ArrayList<Int>
    lateinit var notificationBuilder: NotificationCompat.Builder
    lateinit var notificationManager: NotificationManager


    companion object {
        const val PROGRESS_ACTION = "progress_action"
        const val END_PROCESS_ACTION = "end_process"
        const val PROGRESS_NUMBER = "progress_number"
        var CANCEL = false


    }

    override fun onHandleWork(intent: Intent) {
        val uri = Uri.parse(intent.getStringExtra(MainActivity.FILE_URI))
        algorithms = intent.getIntegerArrayListExtra(MainActivity.CHECKSUM_ALGORITHM)
        val intent2 = Intent(applicationContext, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent2, 0)


        try {
            for (algorithm in algorithms) {
                if (!CANCEL) {
                    notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    notificationBuilder = NotificationCompat.Builder(this, MainActivity.CHANNEL_ID)
                            .setSmallIcon(R.drawable.logo)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentIntent(pendingIntent)
                            .setOngoing(true)
                            .setContentText(getString(R.string.proccesingChecksum))
                    notificationManager.notify(0x021, notificationBuilder.build())
                    val hash = calculateChecksum(algorithm, uri)
                    val i = Intent(END_PROCESS_ACTION)
                    i.putExtra(MainActivity.CHECKSUM_ALGORITHM, algorithm)
                    i.putExtra(MainActivity.HASH, hash)
                    LocalBroadcastManager.getInstance(this@ChecksumService).sendBroadcast(i)

                }
            }
            if(!CANCEL) {
                notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationBuilder = NotificationCompat.Builder(this, MainActivity.CHANNEL_ID)
                        .setSmallIcon(R.drawable.logo)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .setContentText(getString(R.string.finishChecksum))
                notificationManager.notify(0x021, notificationBuilder.build())
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
        CANCEL = true
        super.onTaskRemoved(rootIntent)
    }

    override fun publish(progress: Int, algorithm: Int) {
        val intent = Intent(PROGRESS_ACTION)
        intent.putExtra(PROGRESS_NUMBER, progress)
        intent.putExtra(MainActivity.CHECKSUM_ALGORITHM, algorithm)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)

    }


    fun calculateChecksum(number: Int, uri: Uri): String? {
        val checksum = ChecksumFromFile(this)
        var hash: String? = null
        when (number) {
            1 -> {
                hash = checksum.getMD5Hash(uri)
            }
            2 -> {
                hash = checksum.getSHA1Hash(uri)
            }
            3 -> {
                hash = checksum.getSHA256Hash(uri)
            }
            4 -> {
                hash = checksum.getSHA384Hash(uri)
            }
            5 -> {
                hash = checksum.getSHA512Hash(uri)
            }
            6 -> {
                hash = checksum.getCRC32(uri)
            }
            7 -> {
                hash = checksum.getAdler32(uri)
            }
        }
        return hash
    }
}