package com.jrms.md5checksum.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.jrms.md5checksum.MainActivity

class ChecksumReceiver(var mainActivity: MainActivity) : BroadcastReceiver() {



    override fun onReceive(p0: Context?, p1: Intent?) {

        val action = p1!!.action
        val algorithm = p1.getIntExtra(MainActivity.CHECKSUM_ALGORITHM,0)
        if(action.equals(ChecksumService.PROGRESS_ACTION)){
            val progress = p1.getIntExtra(ChecksumService.PROGRESS_NUMBER, 0)
            mainActivity.changeProgress(algorithm, progress)
        }else{
            val hash = p1.getStringExtra(MainActivity.HASH)
            mainActivity.finishProcess(algorithm, hash)
        }


    }
}