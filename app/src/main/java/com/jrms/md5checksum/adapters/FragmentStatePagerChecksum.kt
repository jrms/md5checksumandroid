package com.jrms.md5checksum.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.util.SparseArray
import android.view.ViewGroup
import com.jrms.md5checksum.fragments.FragmentChecksumFile
import com.jrms.md5checksum.fragments.FragmentChecksumString

class FragmentStatePagerChecksum(fragmentManager: FragmentManager, private val arrayTag : Array<String>)
    : FragmentStatePagerAdapter(fragmentManager) {

    var fragmentSparse = SparseArray<Fragment>()

    override fun getItem(position: Int): Fragment? {
        return when(position){
            0 -> FragmentChecksumFile()
            1 -> FragmentChecksumString()
            else -> null
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return arrayTag[position]
    }


    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val fragment = super.instantiateItem(container, position) as Fragment
        fragmentSparse.put(position, fragment)
        return fragment
    }
}