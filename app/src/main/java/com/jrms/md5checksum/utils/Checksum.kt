package com.jrms.md5checksum.utils


import com.jrms.md5checksum.services.ChecksumService
import org.apache.commons.codec.binary.Hex
import java.io.InputStream
import java.security.MessageDigest
import java.util.zip.Adler32
import java.util.zip.CRC32

abstract class Checksum (val publish: OnPublish? = null){

    interface OnPublish{
        fun publish(progress : Int, algorithm: Int)
    }

    protected fun getHexFromChecksum(algorithm : String, length : Int, inputStream : InputStream, algorithmNo : Int) : String?{
        val messageDigest = MessageDigest.getInstance(algorithm)
        var hashResult : String? = null
        try{
            val bytes = ByteArray(1024)
            var numBytes: Int =  inputStream.read(bytes)
            var currentBytes = 0
            while (numBytes != -1 && !ChecksumService.CANCEL) {
                messageDigest.update(bytes, 0, numBytes)
                currentBytes += numBytes
                val progress : Int = (((currentBytes.toFloat())/(length.toFloat())) * 100).toInt()
                if (publish != null){
                    publish.publish(progress, algorithmNo)
                }
                numBytes =  inputStream.read(bytes)
            }
            val gotHash = messageDigest.digest()
            hashResult = String(Hex.encodeHex(gotHash))
        }catch (e : Exception){
            e.printStackTrace()
        }
        inputStream.close()
        return hashResult
    }

    protected fun getHexFromChecksumCRC32(length : Int, inputStream : InputStream, algorithmNo : Int) : String?{
        val crc32 = CRC32()
        var hashResult : String? = null
        try {
            val bytesBuffer = ByteArray(1024)
            var bytesRead = inputStream.read(bytesBuffer)
            var currentBytes = 0
            while (bytesRead != -1 && !ChecksumService.CANCEL){
                currentBytes += bytesRead
                crc32.update(bytesBuffer, 0, bytesRead)
                val progress : Int = (((currentBytes.toFloat())/(length.toFloat())) * 100).toInt()
                if (publish != null){
                    publish.publish(progress, algorithmNo)
                }
                bytesRead = inputStream.read(bytesBuffer)
            }
            val value = crc32.value
            hashResult = Utils.longToHex(value)
        }catch (e : Exception){
            e.printStackTrace()
        }
        return hashResult
    }

    protected fun getHexFromChecksumAdler32(length : Int, inputStream : InputStream, algorithmNo : Int) : String?{
        val adler32 = Adler32()
        var hashResult : String? = null
        try {
            val bytesBuffer = ByteArray(1024)
            var bytesRead = inputStream.read(bytesBuffer)
            var currentBytes = 0
            while (bytesRead != -1 && !ChecksumService.CANCEL){
                currentBytes += bytesRead
                adler32.update(bytesBuffer, 0, bytesRead)
                val progress : Int = (((currentBytes.toFloat())/(length.toFloat())) * 100).toInt()
                if (publish != null){
                    publish.publish(progress, algorithmNo)
                }
                bytesRead = inputStream.read(bytesBuffer)
            }
            val value = adler32.value
            hashResult = Utils.longToHex(value)
        }catch (e : Exception){
            e.printStackTrace()
        }
        return hashResult
    }


}