package com.jrms.md5checksum.utils;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.jrms.md5checksum.R;
import com.jrms.md5checksum.models.FileMetadata;

public class Utils {
    public static String longToHex(long i){
        return Long.toHexString(i);
    }

    public static FileMetadata getFileMetadata(Context context, Uri uri) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null, null);
        FileMetadata fileMetadata;
        try {
            fileMetadata = new FileMetadata();
            if (cursor != null && cursor.moveToFirst()) {
                fileMetadata.setName(cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)));
                fileMetadata.setFileSize(cursor.getInt(cursor.getColumnIndex(OpenableColumns.SIZE)));
            }
        } catch (Exception e) {
            fileMetadata = null;
        }
        if(cursor != null){
            cursor.close();
        }
        return fileMetadata;
    }

    public static void displayToast(Context context,String message, Toast toast) {
        if (toast == null || !(toast.getView().isShown())) {
            toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    public static void copyToClipboard(Context context, String copyString){
        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText("checksum", copyString);
        if(clipboardManager != null) {
            clipboardManager.setPrimaryClip(clipData);
            Toast.makeText(context, R.string.copiedToClipboard, Toast.LENGTH_SHORT).show();
        }
    }

    public static void closeKeyboard(View view) {
        if (view.requestFocus()) {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            try {
                assert inputMethodManager != null;
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
