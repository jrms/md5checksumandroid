package com.jrms.md5checksum.utils

import java.io.ByteArrayInputStream

class ChecksumFromString : Checksum() {

    fun getMD5Hash( str: String) : String?{

        val stringSize = str.toByteArray(Charsets.UTF_8).size
        val inputStream = ByteArrayInputStream(str.toByteArray(Charsets.UTF_8))
        return getHexFromChecksum("MD5", stringSize, inputStream, 1)
    }

    fun getSHA1Hash(str: String) : String?{

        val stringSize = str.toByteArray(Charsets.UTF_8).size
        val inputStream = ByteArrayInputStream(str.toByteArray(Charsets.UTF_8))
        return getHexFromChecksum("SHA-1", stringSize, inputStream, 2)
    }

    fun getSHA256Hash(str: String) : String?{

        val stringSize = str.toByteArray(Charsets.UTF_8).size
        val inputStream = ByteArrayInputStream(str.toByteArray(Charsets.UTF_8))
        return getHexFromChecksum("SHA-256", stringSize, inputStream, 3)
    }

    fun getSHA384Hash(str: String) : String?{
        val stringSize = str.toByteArray(Charsets.UTF_8).size
        val inputStream = ByteArrayInputStream(str.toByteArray(Charsets.UTF_8))
        return getHexFromChecksum("SHA-384", stringSize, inputStream, 4)
    }

    fun getSHA512Hash(str: String) : String?{
        val stringSize = str.toByteArray(Charsets.UTF_8).size
        val inputStream = ByteArrayInputStream(str.toByteArray(Charsets.UTF_8))
        return getHexFromChecksum("SHA-512", stringSize, inputStream, 5)
    }

    fun getCRC32(str: String) : String?{
        val stringSize = str.toByteArray(Charsets.UTF_8).size
        val inputStream = ByteArrayInputStream(str.toByteArray(Charsets.UTF_8))
        return getHexFromChecksumCRC32( stringSize, inputStream, 6)
    }

    fun getAdler32(str: String) : String?{
        val stringSize = str.toByteArray(Charsets.UTF_8).size
        val inputStream = ByteArrayInputStream(str.toByteArray(Charsets.UTF_8))
        return getHexFromChecksumAdler32( stringSize, inputStream, 7)
    }

}