package com.jrms.md5checksum.utils

import android.net.Uri
import com.jrms.md5checksum.services.ChecksumService

class ChecksumFromFile(val onPublish: OnPublish) : Checksum(onPublish) {
    fun getMD5Hash( uri: Uri) : String?{
        val checksumService = onPublish as ChecksumService
        val fileSize = Utils.getFileMetadata(checksumService, uri).fileSize
        val inputStream = checksumService.contentResolver.openInputStream(uri)
        return getHexFromChecksum("MD5", fileSize, inputStream, 1)
    }

    fun getSHA1Hash(uri: Uri) : String?{
        val checksumService = onPublish as ChecksumService
        val fileSize = Utils.getFileMetadata(checksumService, uri).fileSize
        val inputStream = checksumService.contentResolver.openInputStream(uri)
        return getHexFromChecksum("SHA-1", fileSize, inputStream, 2)
    }

    fun getSHA256Hash(uri: Uri) : String?{
        val checksumService = onPublish as ChecksumService
        val fileSize = Utils.getFileMetadata(checksumService, uri).fileSize
        val inputStream = checksumService.contentResolver.openInputStream(uri)
        return getHexFromChecksum("SHA-256", fileSize, inputStream, 3)
    }

    fun getSHA384Hash(uri: Uri) : String?{
        val checksumService = onPublish as ChecksumService
        val fileSize = Utils.getFileMetadata(checksumService, uri).fileSize
        val inputStream = checksumService.contentResolver.openInputStream(uri)
        return getHexFromChecksum("SHA-384", fileSize, inputStream, 4)
    }

    fun getSHA512Hash(uri: Uri) : String?{
        val checksumService = onPublish as ChecksumService
        val fileSize = Utils.getFileMetadata(checksumService, uri).fileSize
        val inputStream = checksumService.contentResolver.openInputStream(uri)
        return getHexFromChecksum("SHA-512", fileSize, inputStream, 5)
    }

    fun getCRC32(uri: Uri) : String?{
        val checksumService = onPublish as ChecksumService
        val fileSize = Utils.getFileMetadata(checksumService, uri).fileSize
        val inputStream = checksumService.contentResolver.openInputStream(uri)
        return getHexFromChecksumCRC32( fileSize, inputStream, 6)
    }

    fun getAdler32(uri: Uri) : String?{
        val checksumService = onPublish as ChecksumService
        val fileSize = Utils.getFileMetadata(checksumService, uri).fileSize
        val inputStream = checksumService.contentResolver.openInputStream(uri)
        return getHexFromChecksumAdler32( fileSize, inputStream, 7)
    }

}